/*
 * task.js
 *
 * Contains implementation for a "task" "class"
 */

var Task, proto, number;

// Helper method. You should not need to change it.
// Use it in makeTaskFromString
function processString(s) {
   'use strict';
   var tags, title;

   tags = [];
   title = s.replace(/\s*#([a-zA-Z]+)/g, function(m, tag) {
      tags.push(tag);

      return '';
   });

   return { title: title, tags: tags };
}

/*
 *       Constructors
 */

number = 1;

function makeNewTask() {
   var o;

   o = Object.create(proto);
   o.title = '';
   o.completedTime = null;

   Object.defineProperty(o, 'id', {
      enumerable: true,
      configurable: false,
      writable: false,
      value: number
   });
   Object.defineProperty(o, 'tags', {
      enumerable: true,
      configurable: false,
      writable: false,
      value: []
   });
   Object.preventExtensions(o);
   number += 1;

   return o;
}

function makeTaskFromObject(o) {
   var task;

   task = Task.new();
   task.setTitle(o.title);
   task.addTags(o.tags);

   return task;
}

function makeTaskFromString(str) {
   return Task.fromObject(processString(str));
}


/*
 *       Prototype / Instance methods
 */

proto = {
   setTitle: function(s) {
      s = s.trimLeft().trimRight();
      this.title = s;

      return this;
   },
   isCompleted: function() {
      if (this.completedTime === null) {
         return false;
      }

      return true;
   },
   toggleCompleted: function() {
      if (this.isCompleted) {
         this.completedTime = new Date();
      } else {
         this.completedTime = null;
      }

      return this;
   },
   hasTag: function(s) {
      if (this.tags.includes(s)) {
         return true;
      }

      return false;
   },
   addTag: function(s) {
      if (!this.hasTag(s)) {
         this.tags.push(s);
      } else {
         throw new Error('Tag already present.');
      }
   },
   removeTag: function(s) {
      var index;

      if (this.hasTag(s)) {
         index = this.tags.indexOf(s);
         this.tags.splice(index, 1);
      } else {
         throw new Error('Tag does not exist.');
      }

      return this;
   },
   toggleTag: function(s) {
      if (this.hasTag(s)) {
         this.removeTag(s);
      } else {
         this.addTag(s);
      }

      return this;
   },
   addTags: function(arr) {
      var i;

      for (i = 0; i < arr.length; i += 1) {
         if (!this.hasTag(arr[i])) {
            this.addTag(arr[i]);
         }
      }

      return this;
   },
   removeTags: function(arr) {
      var i;

      for (i = 0; i < arr.length; i += 1) {
         if (this.hasTag(arr[i])) {
            this.removeTag(arr[i]);
         }
      }

      return this;
   },
   toggleTags: function(arr) {
      var i;

      for (i = 0; i < arr.length; i += 1) {
         this.toggleTag(arr[i]);
      }

      return this;
   },
   clone: function() {
      var cloneTask;

      cloneTask = Task.new();
      cloneTask.title = this.title;
      cloneTask.completedTime = this.completedTime;
      cloneTask.tags = this.tags;

      return cloneTask;
   }
};


// DO NOT MODIFY ANYTHING BELOW THIS LINE
Task = {
   new: makeNewTask,
   fromObject: makeTaskFromObject,
   fromString: makeTaskFromString
};

Object.defineProperty(Task, 'prototype', {
   value: proto,
   writable: false
});

module.exports = Task;
