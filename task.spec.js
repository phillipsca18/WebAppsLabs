/*
 * task.spec.js
 *
 * Test file for your task class
 */
var expect, Task;

expect = require('./chai.js').expect;

Task = require('./task.js');

// ADD YOUR TESTS HERE
describe('Your code for task class', function() {
   var task = Task.new();

   it('defines a variable Task.', function() {
      expect(function() { Task; }).to.not.throw(Error);
   });
   // it('defines a variable proto.' function() {
   // 	  expect(function() { Task.proto; }).to.not.throw(Error);
   // });
   it('defines a function makeNewTask.', function() {
      expect(Task.new).to.be.a('function');
   });
   it('defines a function makeTaskFromObject.', function() {
   	  expect(Task.fromObject).to.be.a('function');
   });
   it('defines a function makeTaskFromString.', function() {
   	  expect(Task.fromString).to.be.a('function');
   });
});

describe('Your makeNewTask constructor: ', function() {
	var task = Task.new();

	it('returns an object', function() {
      expect(task).to.be.a('object');
    });
    it('returns an object with property id, ', function() {
      expect(task.id).to.be.a('number');
    });
    it('with property title, ', function() {
      expect(task.title).to.be.a('string');
    });
    it('with property completedTime, ', function() {
      expect(task.completedTime).to.be.a('null');
    });
    it('and with property tags.', function() {
      expect(task.tags).to.be.a('array');
    });
});

describe('Your proto object: ', function() {
	var task = Task.new();

	it('returns an object', function() {
	  expect(task).to.be.a('object');
	});
	it('returns an object with methods.', function() {
	  var methods;

	  methods = ['setTitle', 'isCompleted', 'toggleCompleted', 
	  'hasTag', 'addTag', 'removeTag', 'toggleTag', 'addTags', 
	  'removeTags', 'toggleTags', 'clone'];
    methods.forEach(function(key) {
        expect(task[key]).to.be.a('function');
      });
    });
});

describe('Your proto methods: ', function() {
  var task;

  beforeEach(function() {
      task = Task.new();
   });

  it('setTitle correctly sets the given title. ', function () {
    task.setTitle('blah')
    expect(task['title']).to.equal('blah');
  });

  it('isCompleted correctly returns false if the task is not completed. ', function() {
    expect(task['completedTime']).to.equal(null);
    expect(task.isCompleted()).to.equal(false);
  });
  it('toggleCompleted correctly toggles an incomplete task to complete. ', function() {
    task.toggleCompleted();
    expect(task['completedTime']).to.not.equal(null);
    expect(task['completedTime']).to.be.a('Date');
    
  });
  it('isCompleted returns true if the task is completed. ', function() {
    task.toggleCompleted();
    expect(task.isCompleted()).to.equal(true);
  })
  it('hasTag correctly returns false if the tag is not there. ', function() {
    expect(task.hasTag('tag')).to.equal(false);
  });
  it('hasTag correctly returns true if the tag is there. ', function() {
    task.addTag('tag');
    expect(task.hasTag('tag')).to.equal(true);
  });
  it('addTag correctly adds a given tag. ', function() {
    task.addTag('awesome');
    expect(task.hasTag('awesome')).to.equal(true);
  });
  it('addTag throws an error if the tag is already present. ', function() {
    task.addTag('awesome');
    expect(task.hasTag('awesome')).to.equal(true);
    expect(function() {task.addTag('awesome');}).to.throw(Error);
  });
  it('removeTag correctly removes a tag if it is present. ', function() {
    task.addTag('awesome');
    expect(task.hasTag('awesome')).to.equal(true);
    task.removeTag('awesome');
    expect(task.hasTag('awesome')).to.equal(false);
  });
  it('removeTag throws an error if the tag is not present. ', function() {
    expect(function() {task.removeTag('awesome');}).to.throw(Error);
  });
  it('toggleTag correctly adds a tag if it is not present. ', function() {
    task.toggleTag('awesome');
    expect(task.hasTag('awesome')).to.equal(true);
  });
  it('toggleTag correctly removes a tag if it is present. ', function() {
    task.toggleTag('awesome');
    expect(task.hasTag('awesome')).to.equal(true);
    task.toggleTag('awesome');
    expect(task.hasTag('awesome')).to.equal(false);
  });
  it('addTags correctly adds two tags that were not present. ', function() {
    var arr = ['Jon', 'Snow'];
    task.addTags(arr);
    expect(task.hasTag('Jon')).to.equal(true);
    expect(task.hasTag('Snow')).to.equal(true);
    var arr2 = ['Jon', 'Cena'];
    task.addTags(arr2);
    expect(task.hasTag('Cena')).to.equal(true);
  });
  it('addTags correctly adds a tag that was not present and does not add a tag that was already present. ', function() {
    task.addTag('Snow');
    var arr = ['Jon', 'Snow'];
    task.addTags(arr);
    expect(task.hasTag('Jon')).to.equal(true);
    expect(task.hasTag('Snow')).to.equal(true);
  });
  it('removeTags correctly removes two tags that were present. ', function() {
    task.addTag('Jon');
    task.addTag('Snow');
    expect(task.hasTag('Jon')).to.equal(true);
    expect(task.hasTag('Snow')).to.equal(true);
    var arr = ['Jon', 'Snow'];
    task.removeTags(arr);
    expect(task.hasTag('Jon')).to.equal(false);
    expect(task.hasTag('Snow')).to.equal(false);
  });
  it('toggleTags correctly adds one tag and removes another tag. ', function() {
    task.addTag('Jon');
    expect(task.hasTag('Jon')).to.equal(true);
    expect(task.hasTag('Snow')).to.equal(false);
    arr = ['Jon', 'Snow'];
    task.toggleTags(arr);
    expect(task.hasTag('Jon')).to.equal(false);
    expect(task.hasTag('Snow')).to.equal(true);
  });
});

// describe('Your makeTaskFromObject constructor: ', function() {

//     var task = Task.fromObject();

//     it('returns an object', function() {
//       expect(task).to.be.a('object');
//     });
//     it('returns an object with property id, ', function() {
//       expect(task.id).to.be.a('number');
//     });
//     it('with property title, ', function() {
//       expect(task.title).to.be.a('string');
//     });
//     it('with property completedTime, ', function() {
//       expect(task.completedTime).to.be.a('null');
//     });
//     it('and with property tags.', function() {
//       expect(task.tags).to.be.a('array');
//     });
//     it('properly makes a new task from a given object. ', function() {
//       o = {'title': title, 'tags': tags};
//       newTask = task(o);
//     });
// });

// describe('Your makeTaskFromString constructor: ', function () {
//     var task = Task.fromString();

//     it('returns an object', function() {
//       expect(task).to.be.a('object');
//     });
//     it('returns an object with property id, ', function() {
//       expect(task.id).to.be.a('number');
//     });
//     it('with property title, ', function() {
//       expect(task.title).to.be.a('string');
//     });
//     it('with property completedTime, ', function() {
//       expect(task.completedTime).to.be.a('null');
//     });
//     it('and with property tags.', function() {
//       expect(task.tags).to.be.a('array');
//     });
//     // not totally sure how to test this
//     it('properly makes a new task from a given string. ', function() {
//       var string;

//       string = "title tags";
//     });
// });
